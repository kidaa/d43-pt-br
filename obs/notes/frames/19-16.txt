===== The Prophets Warned the People [19-16] =====


{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-19-16.jpg?nolink}}

**God** sent many other **prophets**. They all told the people to stop **worshiping idols** and to **start showing justice and mercy** to others. The prophets warned the people that if they did not stop doing **evil** and start **obeying** God, then God would **judge** them as **guilty**, and he would **punish** them.
===== Important Terms: =====


  * **[[:pt-br:obs:notes:key-terms:god|God]]**
  * **[[:pt-br:obs:notes:key-terms:prophet|prophets]]**
  * **[[:pt-br:obs:notes:key-terms:worship|worshiping]]**
  * **[[:pt-br:obs:notes:key-terms:idol|idols]]**
  * **[[:pt-br:obs:notes:key-terms:justice|justice]]**
  * **[[:pt-br:obs:notes:key-terms:mercy|mercy]]**
  * **[[:pt-br:obs:notes:key-terms:evil|evil]]**
  * **[[:pt-br:obs:notes:key-terms:obey|obeying]]**
  * **[[:pt-br:obs:notes:key-terms:judge|judge]]**
  * **[[:pt-br:obs:notes:key-terms:guilt|guilty]]**
  * **[[:pt-br:obs:notes:key-terms:punish|punish]]**


==== Translation Notes: ====


  * **start showing justice and mercy** - This can be translated as, "start being just and merciful" or, "begin to demonstrate justice and mercy."


**[[:pt-br:obs:notes:frames:19-15|<<]] | [[:pt-br:obs:notes:19|Up]] | [[:pt-br:obs:notes:frames:19-17|>>]]**
