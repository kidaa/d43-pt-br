===== Israel Worshiped Idols [18-12] =====


{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-18-12.jpg?nolink}}

All of the **kings** and most of the people of the **kingdom of Israel worshiped idols**. Their idol worship often included sexual immorality and sometimes even **child sacrifice**.
===== Important Terms: =====


  * **[[:pt-br:obs:notes:key-terms:king|kings]]**
  * **[[:pt-br:obs:notes:key-terms:kingdom-of-israel|kingdom of Israel]]**
  * **[[:pt-br:obs:notes:key-terms:worship|worshiped]]**
  * **[[:pt-br:obs:notes:key-terms:idol|idols]]**
  * **[[:pt-br:obs:notes:key-terms:sacrifice|sacrifice]]**


==== Translation Notes: ====


  * **child sacrifice** - They killed children as offerings to some of their idols.


**[[:pt-br:obs:notes:frames:18-11|<<]] | [[:pt-br:obs:notes:18|Up]] | [[:pt-br:obs:notes:frames:18-13|>>]]**
