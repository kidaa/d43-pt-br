===== The Deliverer [16-03] =====


{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-16-03.jpg?nolink}}

Then **God provided a deliverer** who rescued them from their enemies and **brought peace** to **the land**. But then **the people forgot about God** and started **worshiping idols** again. So God allowed the **Midianites**, a nearby enemy people group, to defeat them.
===== Important Terms: =====


  * **[[:pt-br:obs:notes:key-terms:god|God]]**
  * **[[:pt-br:obs:notes:key-terms:deliverer|deliverer]]**
  * **[[:pt-br:obs:notes:key-terms:peace|peace]]**
  * **[[:pt-br:obs:notes:key-terms:worship|worshiping]]**
  * **[[:pt-br:obs:notes:key-terms:idol|idols]]**
  * **[[:pt-br:obs:notes:key-terms:midian|Midianites]]**


==== Translation Notes: ====


  * **God provided** - This could be translated as, "God chose" or, "God appointed" or, "God raised up."
  * **brought peace** - This could be translated as, "allowed the people to live without fear" or, "ended the fighting" or, "stopped their enemies from attacking them."
  * **the land** - That refers to Canaan, the Promised Land that God had given to Abraham.
  * **the people forgot about God** - This means, "The people stopped thinking about God and ignored what he had commanded them."


**[[:pt-br:obs:notes:frames:16-02|<<]] | [[:pt-br:obs:notes:16|Up]] | [[:pt-br:obs:notes:frames:16-04|>>]]**
