===== A Descendant of David [21-04] =====


{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-21-04.jpg?nolink}}

**God promised King David** that one of his own **descendants** would rule as **king** over God’s people forever. That meant that the **Messiah** would be one of **David’s own descendants**.
===== Important Terms: =====


  * **[[:pt-br:obs:notes:key-terms:god|God]]**
  * **[[:pt-br:obs:notes:key-terms:promise|promised]]**
  * **[[:pt-br:obs:notes:key-terms:david|King David]]**
  * **[[:pt-br:obs:notes:key-terms:messiah|Messiah]]**
  * **[[:pt-br:obs:notes:key-terms:descendant|descendants]]**


==== Translation Notes: ====


  * **David's own descendants** - Another way to say this would be, "a direct descendant of David himself."


**[[:pt-br:obs:notes:frames:21-03|<<]] | [[:pt-br:obs:notes:21|Up]] | [[:pt-br:obs:notes:frames:21-05|>>]]**
