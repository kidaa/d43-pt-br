===== God Loves Sinners [49-07] =====

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-49-07.jpg?nolink}}

**Jesus** taught that **God loves sinners** very much. He wants to **forgive** them and to make them his children.


===== Important Terms: =====

  * **[[:pt-br:obs:notes:key-terms/jesus|Jesus]]**
  * **[[:pt-br:obs:notes:key-terms/god|God]]**
  * **[[:pt-br:obs:notes:key-terms/love|loves]]**
  * **[[:pt-br:obs:notes:key-terms/sin|sinners]]**
  * **[[:pt-br:obs:notes:key-terms/forgive|forgive]]**


===== Translation Notes: =====

  * //(There are no notes for this frame.)// 


**[[:pt-br:obs:notes:frames:49-06|<<]] | [[:pt-br:obs:notes:49|Up]] | [[:pt-br:obs:notes:frames:49-08|>>]]**
